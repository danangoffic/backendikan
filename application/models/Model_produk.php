<?php
/**
 * 
 */
class Model_produk extends CI_Model
{
	public function ambil_produk_penjual($id_akun)
	{
		return $this->db->query("SELECT p.id_produk, p.nama_produk, p.kategori, p.foto_produk, p.harga_produk, p.berat_produk, p.min_pemesanan FROM data_produk p 
				JOIN data_usaha u ON p.id_usaha = u.id_usaha
				WHERE u.id_pj = '$id_akun';");
	}

	public function insert_produk($data)
	{
		return $this->db->insert('data_produk', $data);
	}

	public function insert_variasi_multi($data)
	{
		return $this->db->insert_batch('data_variasi_produk', $data);
	}

	public function insert_variasi($data)
	{
		return $this->db->insert('data_variasi_produk', $data);
	}

	public function ambil_id_variasi($id_var)
	{
		return $this->db->query("SELECT id_variasi FROM data_variasi WHERE nama_variasi = '$id_Var' LIMIT 1;");
	}

	public function hapus($id_produk)
	{
		$this->db->where('id_produk', $id_produk);
		return $this->db->delete('data_produk');
	}

	public function ambil_data_by_id($id_produk)
	{
		$this->db->where('id_produk', $id_produk);
		return $this->db->get('data_produk');
	}

	public function ambil_var_by_produk($id_produk)
	{
		$this->db->where('id_produk', $id_produk);
		return $this->db->get('data_variasi_produk');
	}

	public function ambil_data_variasi()
	{
		return $this->db->query("SELECT * FROM data_variasi ORDER BY id_variasi;");
	}

	public function ambil_variasi_produk($id_produk, $id_variasi)
	{
		$this->db->where('id_produk', $id_produk);
		$this->db->where('id_variasi', $id_variasi);
		return $this->db->get('data_variasi_produk');
	}

	public function ubah_produk($data, $id_produk)
	{
		$this->db->where('id_produk',$id_produk);
		return $this->db->update('data_produk', $data);
	}

	public function hapus_variasi_produk($id_produk)
	{
		$this->db->where('id_produk', $id_produk);
		return $this->db->delete('data_variasi_produk');
	}
}