<?php
/**
 * 
 */
class User extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("Model_user", 'user');
	}

	public function proseslogin()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$cek = $this->user->cek_pengguna($username, $password);
		if($cek->num_rows() > 0){
			$status = 'berhasil';
			$data_pengguna = $cek->row_array();
			$id_akun = $data_pengguna['id_akun'];
			$usergroup = $data_pengguna['level_user'];
			$set_data = array(
						'username' => $data_pengguna['username'],
						'id_akun' => $id_akun);
			$this->session->set_userdata($set_data);
		}else{
			$status = 'gagal';
			$usergroup = '';
			$id_akun = '';
		}
		$response = array(
			'username' => $username,
			'status' => $status,
			'usergroup' => $usergroup,
			'id_akun' => $id_akun
		);
		echo json_encode($response);
	}
}