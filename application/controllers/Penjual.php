<?php
/**
 * 
 */
class Penjual extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		header("Access-Control-Allow-Origin: *");
		$this->load->model('Model_penjual', 'penjual', TRUE);
	}

	public function index()
	{
		echo "Ini adalah class penjual/index";
	}

	public function proses_update()
	{
		$username = $this->input->post('username');
		$ambil_data = $this->penjual->ambil_data_penjual($username);
		print_r($ambil_data->result_array());
	}

	public function ambil_semua()
	{
		$ambil_data = $this->penjual->ambil_semua();
		if($ambil_data->num_rows() > 0){
			$data_json = $ambil_data->result_array();
			header("Content-type: application/json");
			echo json_encode($data_json);
		}else{
			
		}
	}

	public function ambil_data_profile()
	{
		$id_pj = $this->input->post('id_akun');
		$data = $this->penjual->data_profile($id_pj);
		$data_profile = $data->row();
		
		header("Content-type: application/json");
		echo json_encode($data_profile);
	}

	public function ambil_data_kel_tani()
	{
		$id_pj = $this->input->post('id_akun');
		$data = $this->penjual->data_kel_tani_penjual($id_pj);
		$hasil = $data->result_array();
		header("Content-type: application/json");
		echo json_encode($hasil);
	}

	public function ambil_data_tani()
	{
		$id_pj = $this->input->post('id_akun');
		$data_semua_tani = $this->penjual->data_kelompok_tani();
		$data_kel_tani_pj = $this->penjual->data_kel_tani_penjual($id_pj);
		$hasil = array();
		foreach ($data_semua_tani->result() as $key) {
			$cek = $this->penjual->bandingkan_kel_tani_pj($id_pj, $key->id_kelompoktani);
			if($cek->num_rows() > 0){
				$check = "checked='checked'";
			}else{
				$check = "";
			}
			$array = array(
				'id' => $key->id_kelompoktani,
				'label' => $key->nama_kelompoktani,
				'check' => $check);
			array_push($hasil, $array);
		}
		echo json_encode($hasil);
	}

	public function simpan_kel_tani()
	{
		$id_akun = $this->input->post('id_akun');
		$kel_tani = $this->input->post('kel_tani');
		if($kel_tani!==""):
			$kel_tani = explode(',', $kel_tani);
			$total = count($kel_tani);

			$id_usaha = $this->penjual->ambil_data_usaha($id_akun)->row()->id_usaha;

			$delete = $this->penjual->delete_kel_tani($id_usaha);
			if($total>0){
				if($total> 1){
					for ($i=0; $i < $total; $i++) { 
						$data[] = array('id_kelompoktani' => $kel_tani[$i],
							'id_usaha' => $id_usaha);
					}
					$insert = $this->penjual->insert_tani_multi($data);
				}else{
					$data = array('id_kelompoktani' => $kel_tani[0],
						'id_usaha' => $id_usaha);
					$insert = $this->penjual->insert_tani($data);
				}
				if($insert){
					$status = "berhasil";
				}else{
					$status = "gagal";
				}
			}else{
				$status = "kosong";
			}
		else:
			$status = "kosong";
		endif;
		$respons = array('status'=>$status);
		header("Content-type: application/json");
		echo json_encode($respons);
	}

	public function simpan_rekening()
	{
		$bank = $this->input->post('bank');
		$norek = $this->input->post('norek');
		$namarek = $this->input->post('namarek');
		$id_akun = $this->input->post('id_akun');
		$data_ins = array('kode_bank' => $bank, 'id_akun' => $id_akun, 'no_rekening' => $norek, 'nama_rekening' => $namarek);
		$insert = $this->penjual->simpan_rekening($data_ins);
		if($insert){
			$status = "berhasil";
		}else{
			$status = "gagal";
		}

		$respons = array('status' => $status);
		header("Content-type: application/json");
		echo json_encode($respons);
	}

	public function ambil_rekening()
	{
		$id_akun = $this->input->post('id_akun');
		$data = $this->penjual->ambil_rekening_usaha($id_akun);
		$respons = $data->result_array();
		header("Content-type: application/json");
		echo json_encode($respons);
	}

	
}